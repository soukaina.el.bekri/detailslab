<?php

namespace Drupal\details_custom\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class RedirectAnonymousSubscriber.
 */
class RedirectAnonymousSubscriber implements EventSubscriberInterface {


  /**
   * Constructs a new RedirectAnonymousSubscriber object.
   */
  public function __construct() {

  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    $events = [];
    $events[KernelEvents::RESPONSE][] = ['checkAuthStatus', 27];

    return $events;
  }

  /**
   * This method is called whenever the kernel.response event is
   * dispatched.
   *
   * @param \Symfony\Component\EventDispatcher\Event $event $event
   */
  public function checkAuthStatus(Event $event) {
    $account = \Drupal::currentUser();
    $route_name = \Drupal::routeMatch()->getRouteName();

    // Requests allowed for anonymous user
    $allowed_routes = array(
      'user.login',
      'user.pass',
      'user.reset',
      'user.reset.form',
      'user.reset.login'
    );

    // If Anonymous User and Request different than allowed requests, redirect to User Login Page
    if ($account->isAnonymous() && !in_array($route_name, $allowed_routes)) {
      $url = \Drupal\Core\Url::fromRoute('user.login')->setAbsolute()->toString();
      $response = new RedirectResponse($url, 302);
      $response->setMaxAge(0);
      $response->headers->addCacheControlDirective('must-revalidate');
      $response->headers->addCacheControlDirective('no-store');
      $response->headers->addCacheControlDirective('no-cache');
      $event->setResponse($response);
    }
  }
}
