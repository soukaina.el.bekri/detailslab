<?php

namespace Drupal\details_custom\Plugin\migrate\source;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_source_csv\Plugin\migrate\source\CSV;

/**
 * Source for CSV.
 *
 * If the CSV file contains non-ASCII characters, make sure it includes a
 * UTF BOM (Byte Order Marker) so they are interpreted correctly.
 *
 * @MigrateSource(
 *   id = "migration_source_csv"
 * )
 */
class MigrationSourceCSV extends CSV {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);

    if (!empty($this->configuration['module'])) {
      $this->configuration['path'] = drupal_get_path('module', $this->configuration['module']) . '/' . $this->configuration['path'];
    }
  }

}
