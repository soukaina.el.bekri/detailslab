<?php

use Drupal\Core\Render\Markup;
use Drupal\views\Views;

/**
 * On Entity Insert/Update check if it is Resource and if it is to send notification
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 */
function _resource_send_notification(Drupal\Core\Entity\EntityInterface $entity) {
  if ($entity->getEntityTypeId() !== 'node' || ($entity->getEntityTypeId() === 'node' && $entity->bundle() !== 'resource')) {
    return;
  }

  //If Notify by Email is set, send mails and unset field
  if($entity->get('field_rsc_notification')->value){
    $result = _send_email_resource($entity);
    // If notification sent with success, unsset "Notify by Email"
    if ($result == TRUE) {
      $entity->set('field_rsc_notification', 0);
      drupal_register_shutdown_function('_entity_post_insert',$entity);
    }
  }
}

/**
 * Aux function to change entity field value
 *
 * @param $entity
 */
function _entity_post_insert($entity) {
  if($entity) {
    $entity->save();
  }
}

/**
 * Send email about new resource to all agents with Option to receive it
 * @param $resource
 */
function _send_email_resource($resource){
  $mailManager = \Drupal::service('plugin.manager.mail');
  $module = 'details_custom';
  $key = 'new_resource';
  //get emails to send the email with view #Get Users to Notify (get_users_to_notify)
  $view_users_to_notify = Views::getView('get_users_to_notify');
  $view_users_to_notify->execute();
  //Group receivers by langcode. We will send an email per langcode.
  $email_to = array();
  foreach ($view_users_to_notify->result as $row) {
    /** @var \Drupal\user\Entity\User $user */
    $user = $row->_entity;
    $user_langcod = $user->get('langcode')->value;
    $email_to[$user_langcod][] = $user->get('mail')->value;
  }
  //For each lang code send email
  foreach ($email_to as $langcode => $emails ) {
    $to = '';
    $params['resource'] = $resource;
    $params['bcc_mails'] = $emails;
    $send = TRUE;

    $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);

    if ($result['result'] != TRUE) {
      $out_put_message = t('There was an error sending notification to langcode @langcode_mail', array('@langcode_mail' => $langcode));
      \Drupal::logger('mail-log')->error($out_put_message);
      \Drupal::messenger()->addMessage($out_put_message, 'error');
      return FALSE;
    }
    else {
      $out_put_message = t('Notifications sent successfully to langcode @langcode_mail', array('@langcode_mail' => $langcode));
      \Drupal::logger('mail-log')->notice($out_put_message);
      \Drupal::messenger()->addMessage($out_put_message, 'status');
      return TRUE;
    }
  }
}

/**
 * Email message to be sent
 * @param $message
 * @param $params
 */
function _resource_message(&$message, $params){

  $message['from'] = \Drupal::config('system.site')->get('mail');
  $message['headers']['bcc'] = implode(",", $params['bcc_mails']);
  $message['subject'] = t('details Marketing Center | Nouveau contenu');
  $message['body'][] = Markup::create(_get_body_message($params['resource']));
}

/**
 * Message to be sent
 *
 * @param $resource
 * @return string
 */
function _get_body_message($resource) {
    $display_title = $resource->get('field_rsc_display_title')->getValue()[0]['value'];
  $message = t('
  Chers agents,
  <br><br><br>
  Du nouveau contenu a été créé:  <b>@resource_title</b>
  <br>
  <br>
  Il est disponible via le lien suivant: @resource_link
  <br><br><br>
  details Suisse', array('@resource_link' => $resource->toUrl()->setAbsolute()->toString(),
                        '@resource_title' => $display_title));
  return $message;
}
