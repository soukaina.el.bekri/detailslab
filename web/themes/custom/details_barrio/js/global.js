/**
 * @file
 * Global utilities.
 *
 */
(function($, Drupal) {

    'use strict';

    Drupal.behaviors.details_barrio_subtheme = {
        attach: function(context, settings) {
            $(window).scroll(function() {
                if ($(this).scrollTop() > 50) {
                    $('body').addClass("scrolled");
                } else {
                    $('body').removeClass("scrolled");
                }
            });

            //To Top footer link
            $('.to-top').click(function () {
              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
              if (target.length) {
                $('html,body').animate({
                  scrollTop: target.offset().top
                }, 1000);
                return false;
              }
            });
            $(window).bind("mousewheel", function() {
                $("html, body").stop();
            });

        }
    };
    var open=0;
    $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
        e.stopPropagation();
        $(this).next().toggleClass('show');
        $(this).next('.is-active').attr('aria-expanded', true);
       if(location.href!=this.href){
           $(this).attr('aria-expanded', true);
           location.href = this.href;
           open=1;
       }else{
           if($(this).attr('aria-expanded', true) && open===1){
               $(this).removeClass('show');
               $(this).attr('aria-expanded', true);
               open=0;
           }else {
               $(this).addClass('show');
               $(this).attr('aria-expanded', false);
               open=1;
           }
           return false;
       }

        if($(this).hasClass('active')){
            $(this).parents('.dropdown-item').toggleClass('show');
            $(this).attr('aria-expanded',true);
            $(this).next('.is-active').attr('aria-expanded', true);
            $(this).next('.dropdown-menu').toggleClass('show');
        }

        if($(this).next(".dropdown-menu").hasClass('show')){
            $(this).parents('.dropdown-item').toggleClass('show');
            $(this).next('.is-active').attr('aria-expanded', true);
        }


    });
    $(window).on('load',function(){

        if($('li.dropdown-item').hasClass('active')){
            $('li.active').parent().addClass('show');
            $('.dropdown').addClass('show');
            $('.is-active').attr('aria-expanded', true);
            $('.is-active').next().addClass('show');
        }
        $(".active.dropdown-toggle").attr('aria-expanded', true);

    });
    $('#icon-searchform').on('click', function() {
        $('.searchform').css('display','flex');
        $(this).css('display','none');
        if($(window).width() <=992){
            $('.form-item-filter input').animate({width:"15rem"},1000);
        }else{
            $('.form-item-filter input').animate({width:"20rem"},1000);
        }
        $('.form-item-filter input').focus();
    });

    var fields = $('.node--type-resource-set .view-content .field_resource').length;
    for(var i=1; i<=fields; i++) {
        var height = $('#'+i+' .field--name-field-rsc-main-thumbnail').css('height');
        $('#'+i+' .field--name-field-rsc-image-title').css('height',height);
    }


})(jQuery, Drupal);
