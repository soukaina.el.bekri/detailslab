var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var autoprefix = require('gulp-autoprefixer');
var concat = require("gulp-concat");
var glob = require('gulp-sass-glob');
var minifyCss = require("gulp-minify-css");
var uglify = require("gulp-uglify");
var sourcemaps = require('gulp-sourcemaps');
var autoprefixBrowsers = ['> 1%', 'last 3 versions', 'ie 8', 'ie 9', 'ie 10', 'ie 11', 'last 3 Chrome versions', 'last 3 IOS versions', 'last 3 Opera versions', 'last 3 Firefox versions'];

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
    return gulp.src(['scss/style.scss'])
        .pipe(glob())
        // .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.init())
        .pipe(autoprefix({browsers: autoprefixBrowsers}))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest("css"))
        // .pipe(sass({ outputStyle: 'compressed' }))
        // .pipe(minifyCss())
        .pipe(browserSync.stream());
});

// Move the javascript files into our js folder
gulp.task('js', function() {
    return gulp.src(['node_modules/bootstrap/dist/js/bootstrap.min.js', 'node_modules/jquery/dist/jquery.min.js', 'node_modules/popper.js/dist/umd/popper.min.js'])
        .pipe(gulp.dest("js"))
        .pipe(browserSync.stream());
});

const paths = {
	twig: {
		src: ['./templates/**/*.html.twig', 'components/*/*/*.html.twig']
	}
};

// Static Server + watching scss/html files inside Docker
gulp.task('serve', ['sass'], function() {

    browserSync.init({
        proxy: {
          target: "http://ua-details-marketing.details.ch/",
          proxyOptions: {
            changeOrigin: true
          }
        },
        open: false,
    });

    gulp.watch(['scss/*/*/*.scss', 'scss/*/*.scss', 'scss/*.scss', 'components/*/*/*.scss'], ['sass']);
    gulp.watch("src/*.html").on('change', browserSync.reload);
    gulp.watch(paths.twig.src).on('change', browserSync.reload);
});

// Static Server + watching scss/html files on normal Apache
gulp.task('mzserve', ['sass'], function() {

    browserSync.init({
        proxy: {
          target: "http://ua-details-marketing.details.ch/",
        },
        open: false,
    });

    gulp.watch(['scss/*/*/*.scss', 'scss/*/*.scss', 'scss/*.scss'], ['sass']);
    //    gulp.watch("src/*.html").on('change', browserSync.reload);
});

gulp.task('default', ['js', 'serve']);
